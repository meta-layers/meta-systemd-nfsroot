FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# eth.network:
# handled by systemd_%.bbappend - sed magic

# wlan.network:
# removed from here, since meta-wifi-credentials/systemd-networkd
# adds this as well.
# So far I don't want to boot over nfs, not sure if/how this
# is possible at all, since we might need firmware for the Wifi chip 
# which typically resides on the rootfs
# we might do nasty things by looking at the BBFILES variable since
# we could see wheter e.g. meta-wifi-credentials/systemd-networkd is
# included or not - kind of BBLAYERS_DYNAMIC inverse, but than again
# by inversing the logic we could use BBLAYERS_DYNAMIC
# of we could add a FEATURE to tell is if we want to boot over nfs
# and from which interface

#file://eth.network
#file://wlan.network
SRC_URI += " \
    file://en.network \
    file://eth.network \
"

#${sysconfdir}/systemd/network/eth.network
#${sysconfdir}/systemd/network/wlan.network
FILES_${PN} += " \
    ${sysconfdir}/systemd/network/en.network \
"

# package splitting
PACKAGES =+ "${PN}-eth.network"

FILES_${PN}-eth.network += " \
    ${sysconfdir}/systemd/network/eth.network \
"

#install -m 0644 ${WORKDIR}/eth.network ${D}${sysconfdir}/systemd/network
#install -m 0644 ${WORKDIR}/wlan.network ${D}${sysconfdir}/systemd/network
do_install_append() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/en.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/eth.network ${D}${sysconfdir}/systemd/network
}
