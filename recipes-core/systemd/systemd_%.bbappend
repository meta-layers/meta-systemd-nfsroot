PACKAGECONFIG_append = " networkd resolved"

# We need to modify eth.network to be able to boot over nfs:
do_install_append() {
        # we want to insert "KernelCommandLine=!nfsroot" after the line "Name=eth*"
	# but only if the file is available
        if [ -f ${D}${sysconfdir}/systemd/network/eth.network ]; then
        sed -i '/Name=eth*/a KernelCommandLine=!nfsroot' ${D}${sysconfdir}/systemd/network/eth.network
        fi
}

